# Front End

## Getting Started

First, run the docker compose:

```bash
docker compose up
docker exec -it test-product sh 
```

Install package and run the development server: using [`yarn`](https://yarnpkg.com):

```bash
yarn install
yarn dev
```

Or [`npm`](https://www.npmjs.com):

```bash
npm install
npm run dev
```

Open [http://localhost:3002](http://localhost:3002) with your browser to see the result.
