import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import api from "../../utils/instance";
import { IProduct, IParams } from "../../utils/interfaces";
import { openNotificationWithIcon } from "../../utils/config/message";
import {
    MSG_DELETE_SUCCESS,
    MSG_STORE_SUCCESS,
    MSG_UPDATE_SUCCESS,
} from "../../utils/types/messages";
import { RootState } from "../store";
import { imageToBase64 } from "utils/config/helper";
import dayjs from "dayjs";

let today = dayjs();

export const fetchData = createAsyncThunk(
    "product/fetch",
    async (params: IParams, { getState, requestId }) => {
        const response = await api.get("/product", {
            params: {
                ...params,
            },
        });
        return response.data;
    }
);

export const saveOrUpdate = createAsyncThunk(
    "product/save",
    async (data: IProduct, state)  => {
        const appState = state.getState() as RootState;
        const { mode } = appState.product;        
        if (mode === "Add") {
            // @ts-ignore
            const picture = await imageToBase64(data.picture); 
            delete data.id;
            const payload = {
                ...data,
                picture: picture,
            };           
            const response = await api.post("/product", payload);
            return response.data;
        }
        if (mode === "Update") {
            // @ts-ignore
            const picture = data.picture ? await imageToBase64(data.picture) : null;
            const response = await api.put(`/product/${data.id}`, {
                name: data.name,
                qty: data.qty,
                expiredAt: data.expiredAt,
                picture: picture,
            });
            return response.data;
        }
    }
);

export const deleteData = createAsyncThunk(
    "product/delete",
    async (id: string) => {
        const response = await api.delete("/product/" + id);
        return response.data;
    }
);
export interface IPanelProductSlice {
    items: IProduct[];
    form: IProduct;
    total: number;
    modalVisible: boolean;
    loadingFetch: boolean;
    loadingStore: boolean;
    isRefresh: boolean;
    mode: "Add" | "Update" | "Delete";
}
export const defaultProduct = { id: "", name: "", qty: 1, picture: "", expiredAt: "" };
const initialState: IPanelProductSlice = {
    items: [],
    form: defaultProduct,
    total: 0,
    isRefresh: false,
    loadingFetch: true,
    modalVisible: false,
    loadingStore: false,
    mode: "Add",
};

const productSlice = createSlice({
    name: "Product",
    initialState,
    reducers: {
        setMode: (state: { mode: string; }, action: PayloadAction<"Add" | "Update" | "Delete">) => {
            state.mode = action.payload;
        },
        setProduct: (state: { form: any; }, action: PayloadAction<IProduct>) => {
            state.form = action.payload;
        },
        handleModal: (state: { modalVisible: boolean; }, action: PayloadAction<boolean>) => {
            state.modalVisible = action.payload;
        },
    },
    extraReducers: (builder) => {
        // START: fetchData
        builder.addCase(fetchData.pending, (state, action) => {
            state.loadingFetch = true;
        });
        builder.addCase(fetchData.fulfilled, (state, action) => {
            state.loadingFetch = false;
            state.items = action.payload.data.items;
            state.total = action.payload.data.totalItems;
        });

        builder.addCase(fetchData.rejected, (state, action) => {
            state.loadingFetch = false;
        });
        // END: fetchData

        // START: saveOrUpdateEvent
        builder.addCase(saveOrUpdate.pending, (state) => {
            state.loadingStore = true;
        });

        builder.addCase(saveOrUpdate.fulfilled, (state) => {
            state.loadingStore = false;
            state.modalVisible = false;
            state.mode === "Add"
                ? openNotificationWithIcon({
                    type: "success",
                    message: MSG_STORE_SUCCESS,
                })
                : openNotificationWithIcon({
                    type: "success",
                    message: MSG_UPDATE_SUCCESS,
                });
            state.isRefresh = !state.isRefresh;
        });

        builder.addCase(saveOrUpdate.rejected, (state, action) => {
            state.loadingStore = false;
        });
        // END: saveOrUpdateEvent

        // START: deleteData
        builder.addCase(deleteData.pending, (state) => {
            state.loadingStore = true;
        });
        builder.addCase(deleteData.fulfilled, (state) => {
            state.loadingStore = false;
            state.isRefresh = !state.isRefresh;
            openNotificationWithIcon({
                type: "success",
                message: MSG_DELETE_SUCCESS,
            });
        });
        builder.addCase(deleteData.rejected, (state, action) => {
            state.loadingStore = false;
        });
        // END: deleteData
    },
});

export const { setMode, handleModal, setProduct } = productSlice.actions;
export default productSlice.reducer;
