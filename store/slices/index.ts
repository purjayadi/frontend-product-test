import panelSlice from "./panelSlice";
import productSlice from "./productSlice";

export {
  panelSlice,
  productSlice,
};
