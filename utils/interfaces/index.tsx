export interface IParams {
  page?: number;
  limit?: number;
  sort?: string;
  order?: string;
  search?: string;
}

export interface IProduct{
  id?: string;
  name: string;
  qty: number;
  picture: string;
  expiredAt: string;
  isActive?: boolean;
}
