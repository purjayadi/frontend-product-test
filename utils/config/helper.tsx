// date format from date
export function dateFormatFromDate(date: Date): string {
  return new Date(date).toLocaleDateString("id-ID", {
    day: "numeric",
    month: "long",
    year: "numeric",
  });
}

// image to base64
export function imageToBase64(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result as string);
    };
    reader.onerror = (error) => {
      reject(error);
    };
  });
}


// check valid base64
export function isValidBase64(base64: string): boolean {
  return /^data:image\/png;base64,/.test(base64);
}

// string to date
export function stringToDate(str: string): Date {
  return new Date(str);
}

export const paginationProps = {
  showSizeChanger: true,
  pageSizeOptions: ["5", "10", "20", "30", "40", "50"],
  size: "default",
};


