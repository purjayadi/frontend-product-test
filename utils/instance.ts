import axios from "axios";
// Set config defaults when creating the instance

const baseURL: string = "http://localhost/api/v1";
const instance = axios.create({
  baseURL: baseURL,
  timeout: 5000,
  headers: {
    Accept: "application/json",
    ContentType: "application/json",
  },
});

// Add a response interceptor
instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);
export default instance;
