import { CheckSquareOutlined, CloseCircleOutlined, UploadOutlined } from "@ant-design/icons";
import { Checkbox, Col, DatePicker, Form, Image, Input, InputNumber, Modal, Row, Space, Upload } from "antd";
import Button from "antd-button-color";
import React from "react";
import { IProduct } from "utils/interfaces";

interface ModalProps<TProps> {
    data: IProduct;
    mode: string;
    visible: boolean;
    loadingStore: boolean;
    onCancel: () => void;
    handleSubmit: (data: IProduct) => void;
}

export const ModalForm = <TProps,>(props: ModalProps<TProps>) => {
    const [form] = Form.useForm();
    const [picture, setPicture] = React.useState<File | string>("");
    console.log(picture);

    const handleSubmitData = () => {
        form.validateFields().then(values => {
            const payload = {
                ...values,
                picture: picture
            };
            props.handleSubmit(payload);
            setPicture("");
        }).catch(info => {
            console.log("Validate Failed:", info);
        });
    };

    const handleClose = () => {
        setPicture("");
        props.onCancel();
    };

    React.useEffect(() => {
        form.setFieldsValue({
            ...props.data,
        });
    }, [form, props.data]);

    return (
        <Modal
            maskClosable={false}
            destroyOnClose={true}
            title={props.mode}
            visible={props.visible}
            onCancel={handleClose}
            confirmLoading={props.loadingStore}
            footer={[
                <Button
                    key="back"
                    type="danger"
                    onClick={handleClose}
                    icon={<CloseCircleOutlined />}
                >
                    Close
                </Button>,
                <Button
                    key="submit"
                    type="primary"
                    icon={<CheckSquareOutlined />}
                    onClick={handleSubmitData}
                >
                    Submit
                </Button>
            ]}
        >
            <Form
                layout="vertical"
                form={form}
            >
                <Form.Item name="id" noStyle>
                    <Input type="hidden" />
                </Form.Item>
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: "Name is required"
                    }]}
                >
                    <Input placeholder="Product name" />
                </Form.Item>
                <Row>
                    <Col span={8}>
                        <Form.Item
                            name="qty"
                            label="Qty"
                            rules={[{
                                required: true,
                                message: "Qty is required"
                            }]}
                        >
                            <InputNumber placeholder="Qty" style={{ width: "auto" }} />
                        </Form.Item>
                    </Col>
                    <Col span={8} offset={1}>
                        <Form.Item
                            label="Expired Date"
                            name="expiredAt"
                            rules={[{
                                required: true,
                                message: "Expired date required"
                            }]}
                        >
                            {/* Bug styled component */}
                            {/* @ts-ignore */}
                            <DatePicker
                                format={"DD/MM/YYYY"}
                                style={{ width: "100" }}
                                placeholder="Select date"
                            />
                        </Form.Item>
                    </Col>
                </Row>


                <Form.Item
                    label="Picture"
                >
                    <Upload
                        accept="image/png, image/jpeg"
                        maxCount={1}
                        beforeUpload={(file) => {
                            setPicture(file);
                            return false;
                        }}
                        listType="picture"
                    >
                        <Button color="dark">
                            <UploadOutlined />
                            Upload
                        </Button>
                    </Upload>
                    {
                        props.mode === "Update" && <>
                            <Image
                                alt="product"
                                width={100}
                                src={`data:image/png;base64,${props.data.picture}`} 
                            />
                            <Form.Item name="isActive">
                                <Checkbox checked={props.data.isActive}>Active</Checkbox>
                            </Form.Item>
                        </>
                    }
                </Form.Item>
            </Form>
        </Modal>
    );
};