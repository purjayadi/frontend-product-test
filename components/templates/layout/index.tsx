import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Col, Layout, Menu, Row } from "antd";
import Image from "next/image";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "utils/hooks";
import {
  IPanelSlice,
  setOpenKeys,
  setSelectedKeys,
} from "../../../store/slices/panelSlice";
import { items } from "./menu";

interface IProps {
  children: React.ReactNode;
}

const { Header, Content, Footer, Sider } = Layout;
function AdminLayout({ children }: IProps) {
  const [collapsed, setCollapsed] = useState(false);  

  const state: IPanelSlice = useSelector((state: any) => state.panel);
  const dispatch = useAppDispatch();

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <Layout hasSider style={{padding: 0}}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <Row justify="center" align="middle" style={{minHeight: "8.2vh"}}>
          <Image
            src="/assets/images/logo.svg"
            width={40}
            height={40}
            objectFit="cover"
            alt="logo"
          />
        </Row>
        <Menu
          theme="dark"
          mode="inline"
          onClick={({ key, keyPath }) => {
            dispatch(setSelectedKeys([key]));
            dispatch(setOpenKeys(keyPath));
          }}
          onOpenChange={(openKeys) => {
            dispatch(setOpenKeys(openKeys));
          }}
          selectedKeys={state.selectedKeys}
          openKeys={state.openKeys}
          defaultOpenKeys={state.openKeys}
          defaultSelectedKeys={state.selectedKeys}
          items={items}
        />
        
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
        >
        </Header>
        <Content
          style={{ margin: "24px 16px 0"}}
        >
          <div className="site-layout-background" style={{ padding: 24, minHeight: 600 }}>
          {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
}

export default AdminLayout;
