import { DeleteOutlined, ExclamationCircleOutlined, FormOutlined } from "@ant-design/icons";
import { Modal, Space } from "antd";
import React from "react";
import { MSG_CONFIRM_DELETE } from "utils/types";
import Button from "antd-button-color";
const { confirm } = Modal;

interface Props {
  handleEdit: () => void;
  handleDelete: () => void;
}

// @ts-ignore
const RenderButtonAction: React.FC<Props> = ({ handleDelete, handleEdit }) => {

  const handleDeleteConfirm = () => {
      confirm({
        title: "Delete Data",
        content: MSG_CONFIRM_DELETE,
        icon: <ExclamationCircleOutlined />,
        okText: "Yes",
        okType: "danger",
        cancelText: "No",
        onOk() {
          handleDelete();
        },
        onCancel() {
          console.log("Cancel");
        },
      });
  };

  return (
    <Space>
      <Button type="info" size="small" onClick={handleEdit}>
        <FormOutlined />
      </Button>
      <Button type="danger" size="small" onClick={handleDeleteConfirm}>
        <DeleteOutlined />
        
      </Button>
    </Space>
  );
};

export default RenderButtonAction;
