/* eslint-disable @next/next/no-sync-scripts */
import type { AppProps } from "next/app";
import Head from "next/head";
import NextNProgress from "nextjs-progressbar";
import React from "react";
import "../styles/antd.css";
import { Provider } from "react-redux";
import { store } from "../store/store";

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  return (
    <Provider store={store}>
        <Head>
          <title>Product</title>
        </Head>
        <NextNProgress
          color="#3498db"
          startPosition={0.3}
          stopDelayMs={200}
          height={3}
          showOnShallow={true}
          options={{
            easing: "ease",
            speed: 500,
          }}
        />
        {/* @ts-ignore */}
        <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
