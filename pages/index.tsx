import React from "react";
import { Button, Card, Image, Tag, Space, Select, Row, Col } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useAppDispatch, useAppSelector } from "utils/hooks";
import { RootState } from "store/store";
import { AdminLayout } from "components/templates";
import { IProduct } from "utils/interfaces";
import { defaultProduct, deleteData, fetchData, handleModal, saveOrUpdate, setMode, setProduct } from "store/slices/productSlice";
import Table, { ColumnsType } from "antd/lib/table";
import { RenderButtonAction } from "components/atoms";
import { dateFormatFromDate } from "utils/config/helper";
import Search from "antd/lib/input/Search";
import { ModalForm } from "components/organisms/modalForm";
import moment from "moment";

const Index = () => {
  const dispatch = useAppDispatch();
  const product = useAppSelector((state: RootState) => state.product);
  const [page, setPage] = React.useState<number>(1);
  const [limit, setLimit] = React.useState<number>(5);
  const [sort, setSort] = React.useState<string>("createdAt");
  const [order, setOrder] = React.useState<string>("DESC");
  const [search, setSearch] = React.useState<string>("");
  
  React.useEffect(() => {
    dispatch(fetchData({ page, limit, sort, order, search }));
  }, [dispatch, page, limit, product.isRefresh, sort, order, search]);
  
  const handleSubmit = (values: IProduct) => {
    dispatch(saveOrUpdate(values));
  };
  const handleCreate = () => {
    dispatch(setMode("Add"));
    dispatch(setProduct({
      ...defaultProduct
    }));
    dispatch(handleModal(true));
  };

  const handleEdit = (data: IProduct) => {
    dispatch(setMode("Update"));
    dispatch(setProduct({
      ...data,
      // @ts-ignore
      expiredAt: moment(data.expiredAt)
    }));
    dispatch(handleModal(true));
  };

  const handleDelete = (id: string) => {
    console.log(id);
    
    dispatch(deleteData(id));
  };

  const columns: ColumnsType<IProduct> = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: {}
    },
    {
      title: "Qty",
      dataIndex: "qty",
      key: "qty",
      sorter: {},
      responsive: ["lg"],
    },
    {
      title: "Picture",
      dataIndex: "picture",
      key: "picture",
      responsive: ["lg", "md"],
      sorter: {},
      render: (text: any, record: IProduct) => {
        return <Image
          alt="product"
          width={100}
          src={`data:image/png;base64,${record.picture}`}
        />;
      }
    },
    {
      title: "Status",
      dataIndex: "isActive",
      key: "isActive",
      responsive: ["lg"],
      sorter: {

      },
      render: (text: boolean, record: IProduct) => {
        return record.isActive ? <Tag color="#87d068">Active</Tag> : <Tag color="#f50">Inactive</Tag>;
      }
    },
    {
      title: "Expired At",
      dataIndex: "expiredAt",
      key: "expiredAt",
      responsive: ["lg"],
      sorter: {

      },
      render: (expiredAt) => {
        return dateFormatFromDate(expiredAt);
      }
    },
    {
      title: "Action",
      key: "action",
      render: (text: any, record: IProduct) => (
        <RenderButtonAction
          handleEdit={() => handleEdit(record)}
          handleDelete={() => handleDelete(record.id!)}
        />
      ),
    },
  ];

  const onChange = (pagination: any, filters: any, sorter: any, extra: any) => {
    setSort(sorter.field);
    sorter.order === "ascend" ? setOrder("ASC") : setOrder("DESC");
  };

  return (
    <AdminLayout>
      <Card
        title="Products"
        extra={
          <Button type="primary" onClick={handleCreate}>
            <PlusOutlined /> Add
          </Button>
        }
      >
        <Space direction="vertical" style={{ display: "flex" }}>
          <Row>
            <Col xs={{ span: 12 }} lg={{ span:6, offset: 18 }}>
              <Search placeholder="Search product" onSearch={(val) => setSearch(val)} />
            </Col>
          </Row>
          <Table
            size="small"
            onChange={onChange}
            showHeader={true}
            loading={product.loadingFetch}
            columns={columns}
            rowKey={(record: IProduct) => record.id!}
            dataSource={product.items}
            pagination={{
              defaultPageSize: limit,
              total: product.total,
              showSizeChanger: true,
              pageSizeOptions: ["5", "10", "20", "30", "40", "50"],
              size: "default",
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} items`,
              onChange: (page: number, limit: number) => {
                setPage(page);
                setLimit(limit);
              },
            }}
          />
        </Space>
      </Card>
      <ModalForm
        visible={product.modalVisible}
        onCancel={() => dispatch(handleModal(false))}
        handleSubmit={handleSubmit}
        mode={product.mode}
        data={product.form}
        loadingStore={product.loadingStore}
      />
    </AdminLayout>
  );
};

export default Index;